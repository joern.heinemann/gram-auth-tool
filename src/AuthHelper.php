<?php
namespace Joern\Auth_Tools;

use Joern\Auth_Tools\User\UserInterface;

/**
 * Class AuthHelper
 * @package Joern\Auth_Tools
 *
 * Hilfsklasse
 * für Password handling
 */
class AuthHelper
{
	const PW_ALOG=PASSWORD_DEFAULT;
	const PW_COST = ['cost'=>12];

	/**
	 * Erstellt einen Pw hash
	 *
	 * zu den angegebenen Conditions
	 *
	 * @param $pw
	 * @return bool|string
	 */
	public static function passwordHash($pw)
	{
		return \password_hash($pw,self::PW_ALOG,self::PW_COST);
	}

	/**
	 * Prüfe anhand der Conditions ob das eingegebene Ow mit dem des Users über einstimmt
	 *
	 * @param UserInterface $user
	 * @param $password
	 * @return bool
	 */
	public static function passwordValid(UserInterface $user, $password)
	{
		return \password_verify($password,$user->getPassword());
	}
}