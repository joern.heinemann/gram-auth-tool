<?php
namespace Joern\Auth_Tools\Authentication;

use Gram\Project\Lib\Cookie\Psr7CookieInterface;
use Gram\Project\Lib\Session\SessionInterface;
use Joern\Auth_Tools\AuthHelper;
use Joern\Auth_Tools\User\UserInterface;
use Joern\Auth_Tools\User\UserModelInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class SimpleAuthentication
 * @package Joern\Auth_Tools\Authentication
 */
class SimpleAuthentication
{
	/** @var UserModelInterface */
	protected $model;

	/** @var Psr7CookieInterface */
	protected $cookie;

	/** @var UserInterface */
	protected $user;

	/** @var SessionInterface */
	protected $session;

	/** @var ServerRequestInterface */
	protected $request;

	/** @var ResponseInterface|null */
	protected $response;

	/**
	 * SimpleAuthentication constructor.
	 * @param UserModelInterface $model
	 * @param Psr7CookieInterface $cookie
	 * @param SessionInterface $session
	 * @param ServerRequestInterface $request
	 * @param ResponseInterface|null $response
	 */
	public function __construct(
		UserModelInterface $model,
		Psr7CookieInterface $cookie,
		SessionInterface $session,
		ServerRequestInterface $request,
		ResponseInterface $response = null
	) {
		$this->model = $model;
		$this->cookie = $cookie;
		$this->session = $session;
		$this->request = $request;
		$this->response = $response;
	}

	/**
	 * Gebe Informationen über den user
	 * in Form eines Objects zurück
	 *
	 * @return UserInterface
	 */
	public function getUser(): UserInterface
	{
		return $this->user;
	}

	/**
	 * Loggt den User ein
	 * incl cookie
	 *
	 * @param string $userName
	 * @param string $password
	 * @param bool $cookie
	 * @return array
	 */
	public function login(string $userName, string $password, bool $cookie = false)
	{
		$this->user = $this->model->selectUserByName($userName);

		if (!$this->user->status() || !AuthHelper::passwordValid($this->user,$password)) {
			//User nicht gefunden oder password falsch
			return [false, null];
		}

		$this->setSession();

		if(!$cookie) {
			//Kein Cookie
			return [true, null];
		}

		return [$this->generateCookie(true),$this->response];
	}

	/**
	 * Prüfe ob der User eingeloggt ist
	 *
	 * Prüfe auch cookie
	 *
	 * @param bool $cookie
	 * @return array(status,response)
	 * status:
	 * -> 0 = Nicht eingeloggt
	 * -> 1 = per Session eingeloggt
	 * -> 2 = per cookie eingeloggt
	 *
	 * gebe einen Psr 7 Response mit dem erneuerten cookie zurück
	 * oder ein array mit den raw cookie
	 */
	public function isLoggedIn(bool $cookie)
	{
		if($this->session->keyExist('user','username') && $this->session->keyExist('user','userid')) {
			//per Session angemeldet
			return [1,null];
		}

		if(!$cookie) {
			return [0,null];
		}

		//auf cookie prüfen
		$seesionid = $this->cookie->get($this->request,'sessionid');
		$token = $this->cookie->get($this->request,'token');

		if(!$token || !$seesionid){
			return [0,null];
		}

		//hole user infos und prüfe gleichzeitig ob es den user mit dem gerät und dem token gibt

		$this->user = $this->model->selectUserByCookie($seesionid,$token);

		if(!$this->user->status()){
			return [0,null];
		}

		//erstellt nach jedem Cookie login ein neues Token und session id

		//Speichere neuen Cookie
		if(!$this->generateCookie()){
			return [0,null];
		}

		//setze Werte in die Session
		$this->setSession();

		return [2,$this->response];
	}

	/**
	 * Loggt einen user aus und löscht seine Session
	 *
	 * @return array
	 */
	public function logout()
	{
		$userid = $this->session->get('user','userid');

		//Werte aus DB löschen
		$this->model->deleteSession($userid,$this->cookie->get($this->request,'sessionid'));

		//Cookie löschen
		$this->response = $this->cookie->delete($this->response,'sessionid');
		$response = $this->cookie->delete($this->response,'token');

		$this->session->destroy();

		$destroy = (!$this->session->keyExist('user','username'));

		return [$destroy,$response];
	}

	/**
	 * Erstellt den Cookie
	 *
	 * @param bool $new
	 * @return bool
	 */
	protected function generateCookie(bool $new = false)
	{
		$sessionId = $this->userSessionId();
		$token = $this->userToken();

		//wenn user zum ersten mal einen Cookie setzt

		if($new){
			//DB Eintrag
			if(!$this->model->insertCookie($this->user->getId(),$sessionId,$token)){
				return false;
			}
		}else{
			$oldsid = $this->cookie->get($this->request,'sessionid');
			$oldtoken = $this->cookie->get($this->request,'token');

			//DB Eintrag

			if(!$this->model->setSession($this->user->getId(),$oldsid,$sessionId) ||
				!$this->model->setToken($this->user->getId(),$oldtoken,$token)
			) {
				return false;
			}
		}

		//wenn noch kein Response erzeugt wurde
		//muss nachträglich in Response eingesetzt werden mit withAddedHeader()
		if($this->response === null){
			$this->response = [
				$this->cookie->setRaw('token',$token),
				$this->cookie->setRaw('sessionid',$sessionId)
			];
		}else{
			$this->response = $this->cookie->set($this->response,'token',$token);
			$this->response = $this->cookie->set($this->response,'sessionid',$sessionId);
		}

		return true;
	}

	/**
	 * Setze Werte in die Session
	 *
	 * Anhand dieser Werte wird der User per Session authentifiziert
	 */
	protected function setSession()
	{
		$this->session->set('user',[
			'username'=>$this->user->getName(),
			'userid'=>$this->user->getId()
		]);
	}

	protected function userSessionId(){
		//erstelle sessionid
		do{
			$sessionId = $this->generateToken();
		}while($this->model->sessionIdExist($sessionId));	//wiederhole solange bis ein neues Token gefunden wurde

		return true;
	}

	protected function userToken(){
		//erstelle token
		do{
			$token = $this->generateToken();
		}while($this->model->tokenExist($token));	//wiederhole solange bis ein neues Token gefunden wurde

		return true;
	}

	protected function generateToken()
	{
		try {
			$token = bin2hex(random_bytes(10));
		} catch (\Exception $e) {
			echo $e;
			return false;
		}

		return $token;		//gebe Token zurück um es ins form ein zubinden
	}
}