<?php
/**
 * @author Jörn Heinemann <joernheinemann@gmx.de>
 * @since 2020/04/17
 */

namespace Joern\Auth_Tools\User;

use Gram\Project\Lib\DB\DBInterface;

/**
 * Class UserModel
 * @package Joern\Auth_Tools\User
 *
 * Die Implementation für Cookie
 *
 * Setzt die Tabelle user_auth voraus
 * Diese kann mithilfe von @see \Joern\Auth_Tools\User\Sql\User_Auth_DBInsert
 * einfach hinzugefügt werden
 */
abstract class UserModel implements UserModelInterface
{
	/** @var DBInterface */
	protected $db;

	/**
	 * UserModel constructor.
	 * @param DBInterface $db
	 */
	public function __construct(DBInterface $db)
	{
		$this->db = $db;
	}

	/**
	 * @inheritdoc
	 */
	public function insertCookie($userId, $sessionId, $token)
	{
		$sql="Insert INTO user_auth (userid, session, token) VALUES (:userid, :seesion, :token)";

		return $this->db->query($sql, [
			'seesion'=>$sessionId,
			'userid'=>$userId,
			'token'=>$token
		]);
	}

	/**
	 * @inheritdoc
	 */
	public function tokenExist($token): bool
	{
		return $this->db->exist("user_auth","token LIKE ?", [$token]);
	}

	/**
	 * @inheritdoc
	 */
	public function sessionIdExist($sessionId): bool
	{
		return $this->db->exist("user_auth","session LIKE ?", [$sessionId]);
	}

	/**
	 * @inheritdoc
	 */
	public function setSession($userId, $oldSessionId, $sessionId): bool
	{
		$sql="UPDATE user_auth SET session = :session WHERE userid LIKE :userid AND session LIKE :oldsid";

		return $this->db->query($sql, [
			'session'=>$sessionId,
			'userid'=>$userId,
			'oldsid'=>$oldSessionId
		]);
	}

	/**
	 * @inheritdoc
	 */
	public function setToken($userId, $oldtoken, $token): bool
	{
		$sql="UPDATE user_auth SET token = :token WHERE userid LIKE :userid AND token LIKE :oldt";

		return $this->db->query($sql, [
			'token'=>$token,
			'userid'=>$userId,
			'oldt'=>$oldtoken
		]);
	}

	/**
	 * @inheritdoc
	 */
	public function deleteSession($userid, $sessionid): bool
	{
		$sql="DELETE FROM user_auth WHERE userid LIKE ? AND session LIKE ?";

		return $this->db->query($sql, [$userid,$sessionid]);
	}
}