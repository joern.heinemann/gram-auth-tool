<?php
namespace Joern\Auth_Tools\User;

/**
 * Interface UserModelInterface
 * @package Joern\Auth_Tools\User
 *
 * Dieses Model beschreibt wie die Informationen
 * über den user erstellt werden
 */
interface UserModelInterface
{
	/**
	 * Erstelle einen user anhand seiner Id
	 *
	 * @param $id
	 * @return UserInterface
	 */
	public function selectUserById($id): UserInterface;

	/**
	 * Erstelle einen user anhand seines eindeutigen username
	 *
	 * @param string $name
	 * @return UserInterface
	 */
	public function selectUserByName(string $name): UserInterface;

	/**
	 * Erstelle einen user anhand eines cookies
	 *
	 * Cookie besteht aus zwei Elementen: die Session id und ein token
	 * beide sind number only once
	 *
	 * @param string $sessionId
	 * @param string $token
	 * @return UserInterface
	 */
	public function selectUserByCookie(string $sessionId, string $token): UserInterface;

	/**
	 * Füge einen neuen Cookie hinzu
	 *
	 * Es ist besser cookies in einer separaten Tabelle zu speichern,
	 * da der User auf unterschiedlichen clients cookies abspeichern kann
	 *
	 * @param $userId
	 * @param $sessionId
	 * @param $token
	 * @return mixed
	 */
	public function insertCookie($userId, $sessionId, $token);

	/**
	 * Prüfe ob ein Token für den Cookie existiert
	 *
	 * Token muss number only once sein
	 *
	 * @param $token
	 * @return bool
	 */
	public function tokenExist($token):bool;

	/**
	 * Prüfe ob eine Session Id für den Cookie existiert
	 *
	 * Session muss number only once sein
	 *
	 * @param $sessionId
	 * @return bool
	 */
	public function sessionIdExist($sessionId):bool;

	/**
	 * Speichere eine Session Id für den User
	 *
	 * benötigt für cookies
	 *
	 * session Id muss number only once sein
	 *
	 * (siehe cookie hinweis bei @see insertCookie )
	 *
	 * @param $userId
	 * @param $oldSessionId
	 * @param $sessionId
	 * @return bool
	 */
	public function setSession($userId, $oldSessionId, $sessionId):bool;

	/**
	 * Speichere ein Token für den User
	 *
	 * benötigt für cookies
	 *
	 * Token muss number only once sein
	 *
	 * (siehe cookie hinweis bei @see insertCookie )
	 *
	 * @param $userId
	 * @param $oldtoken
	 * @param $token
	 * @return bool
	 */
	public function setToken($userId, $oldtoken, $token):bool;

	/**
	 * Löscht einen Cookie des Users
	 *
	 * @param $userid
	 * @param $sessionid
	 * @return bool
	 */
	public function deleteSession($userid,$sessionid):bool;

	/**
	 * Prüft ob es einen User mit dem usernamen gibt
	 *
	 * @param string $name
	 * @return bool
	 */
	public function userExist(string $name): bool;
}