<?php
namespace Joern\Auth_Tools\User;

/**
 * Class User
 * @package Joern\Auth_Tools\User
 *
 * Einfache Basis Implementation eines Users
 */
class User implements UserInterface
{
	/** @var mixed */
	protected $id;

	/** @var string */
	protected $userName;

	/** @var string */
	protected $password;

	/**
	 * User constructor.
	 * @param $id
	 * @param string $userName
	 * @param string $password
	 */
	public function __construct($id, string $userName, string $password)
	{
		$this->id = $id;
		$this->userName = $userName;
		$this->password = $password;
	}

	/**
	 * @inheritdoc
	 */
	public function status(): bool
	{
		return !($this->id === false || $this->userName === false || $this->password === false);
	}

	/**
	 * @inheritdoc
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @inheritdoc
	 */
	public function getName(): string
	{
		return $this->userName;
	}

	/**
	 * @inheritdoc
	 */
	public function getPassword(): string
	{
		return $this->password;
	}
}