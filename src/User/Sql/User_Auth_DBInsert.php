<?php
/**
 * @author Jörn Heinemann <joernheinemann@gmx.de>
 * @since 2020/04/17
 */

namespace Joern\Auth_Tools\User\Sql;

use Gram\Project\Lib\DB\DBInterface;

/**
 * Class User_Auth_DBInsert
 * @package Joern\Auth_Tools\User\Sql
 *
 * Füge die Tabelle user_auth hinzu
 *
 * Diese wird für Cookies benötigt
 *
 * Mit dieser Tabelle kann die Klasse @see \Joern\Auth_Tools\User\UserModel
 * genutzt werden
 */
class User_Auth_DBInsert
{
	/** @var DBInterface */
	private $db;

	/**
	 * User_Auth_DBInsert constructor.
	 * @param DBInterface $DB
	 */
	public function __construct(DBInterface $DB)
	{
		$this->db = $DB;
	}

	/**
	 * @return string
	 */
	public function __invoke()
	{
		$sql = "CREATE TABLE `user_auth` (
					`id` int(11) NOT NULL,
					`userId` int(11) NOT NULL,
					`session` text NOT NULL,
					`token` text NOT NULL
				) ENGINE=InnoDB DEFAULT CHARSET=utf8;
				
				--
				-- Indizes der exportierten Tabellen
				--
				
				--
				-- Indizes für die Tabelle `user_auth`
				--
				ALTER TABLE `user_auth`
					ADD PRIMARY KEY (`id`),
					ADD KEY `userId` (`userId`);
				
				--
				-- AUTO_INCREMENT für exportierte Tabellen
				--
				
				--
				-- AUTO_INCREMENT für Tabelle `user_auth`
				--
				ALTER TABLE `user_auth`
					MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
				COMMIT;";

		return $this->db->query($sql)."";	//Gebe einen streing zurück
	}
}