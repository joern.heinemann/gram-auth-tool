CREATE TABLE `user_auth` (
	`id` int(11) NOT NULL,
	`userId` int(11) NOT NULL,
	`session` text NOT NULL,
	`token` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `user_auth`
--
ALTER TABLE `user_auth`
	ADD PRIMARY KEY (`id`),
	ADD KEY `userId` (`userId`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `user_auth`
--
ALTER TABLE `user_auth`
	MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;