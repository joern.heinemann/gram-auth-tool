<?php
namespace Joern\Auth_Tools\User;

/**
 * Interface UserInterface
 * @package Joern\Auth_Tools\User
 *
 * Ein object, dass die Informationen des Users enthält
 */
interface UserInterface
{
	/**
	 * Gebe an ob die Informationen richtig gefetcht wurden
	 *
	 * @return bool
	 */
	public function status(): bool;

	/**
	 * Id des Users
	 *
	 * @return mixed
	 */
	public function getId();

	/**
	 * Unique User name
	 *
	 * @return string
	 */
	public function getName(): string;

	/**
	 * Gehashte Password
	 *
	 * @return string
	 */
	public function getPassword(): string;
}