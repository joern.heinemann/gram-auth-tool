<?php
namespace Joern\Auth_Tools\Middleware;

use Gram\Mvc\Lib\Controller\BaseController;
use Gram\Project\Lib\Cookie\Psr7CookieInterface;
use Joern\Auth_Tools\Authentication\SimpleAuthentication;
use Joern\Auth_Tools\User\UserModelInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Class Authenticate
 * @package Joern\Auth_Tools\Middleware
 *
 * Eine Psr 15 Middleware
 *
 * Führt die Authentication mithilfe der SimpleAuthentication Class druch
 */
class Authenticate implements MiddlewareInterface
{
	/** @var callable|null */
	protected $requestHandling;

	/** @var callable|null */
	protected $sessionHandling;

	/** @var UserModelInterface */
	protected $model;

	/** @var Psr7CookieInterface */
	protected $cookie;

	/**
	 * Authenticate constructor.
	 * @param UserModelInterface $model
	 * @param Psr7CookieInterface $cookie
	 * @param callable|null $sessionHandling
	 * @param callable|null $requestHandling
	 */
	public function __construct(
		UserModelInterface $model,
		Psr7CookieInterface $cookie,
		callable $sessionHandling = null,
		callable $requestHandling = null
	) {
		$this->model = $model;
		$this->cookie = $cookie;
		$this->requestHandling = $requestHandling;
		$this->sessionHandling = $sessionHandling;
	}

	/**
	 * @inheritdoc
	 *
	 *
	 */
	public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
	{
		$session = BaseController::getSessionClass($request);

		$auth = new SimpleAuthentication($this->model,$this->cookie,$session,$request);

		//ohne response, da dieser noch nicht exsitiert
		[$login, $responseHeaders] = $auth->isLoggedIn(true);

		if($login === 0){
			$request = $request->withAttribute('login',false);
		} else {
			$request = $request->withAttribute('login',true);
		}

		//weitere werte in die Session speichern, wenn mit cookie angemeldet wurde
		if($login === 2){
			//Custom values in die Session packen
			if(isset($this->sessionHandling)) {
				\call_user_func($this->sessionHandling,$auth->getUser(),$session);
			}

			$login = 1;
		}

		if($login === 1) {
			//Wenn über die Session eingeloggt, speichere custom values in den request
			if(isset($this->requestHandling)) {
				$request = \call_user_func($this->requestHandling, $request, $session);
			}
		}

		//nehme Response entgegen
		$response = $handler->handle($request);

		if($responseHeaders === null) {
			//wenn keine Cookies gesetzt sind weiter geben
			return $response;
		}

		//sonst ändere sessionId und token

		foreach ($responseHeaders as $responseHeader) {
			$response = $this->cookie->setRawCookie($response,$responseHeader);
		}

		return $response;
	}
}